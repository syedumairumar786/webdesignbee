<?php
/**
 * The template for displaying search results pages
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 */

get_header(); ?>

	<div class="content">

		<div class="inner-content grid-x grid-margin-x grid-padding-x">

			<main class="main large-12 cell" role="main">

        <!-- Main Section -->
         <section class="section" id="main">
         <div class="grid-container">
           <div class="grid-x grid-margin-x grid-padding-x">
             <div class="large-12 cell">

               <div class="section-title">
                 <header>
         					<h3 class="archive-title"><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h3>
         				</header>
               </div>
             </div>
           </div>
         </div>
         </section>
		<section id="articles" class="section">
			<div class="grid-container">
				<div class="grid-x grid-margin-x grid-padding-x">
					<div class="large-12 cell">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<!-- To see additional archive styles, visit the /parts directory -->
							<?php get_template_part( 'parts/loop', 'archive' ); ?>

						<?php endwhile; ?>

							<?php joints_page_navi(); ?>

						<?php else : ?>

							<?php get_template_part( 'parts/content', 'missing' ); ?>

							<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
      <section id="service">
       <div class="grid-container">
         <div class="grid-x grid-margin-x grid-padding-x">
           <div class="large-12 cell">
             <div class="section-info">
               <div class="section-title">
                 <h2>Website running slow?</h2>
               </div>
               <div class="section-content">
                 <div class="grid-x">
                   <div class="large-8 cell">
                     <div class="service-content">
                       <p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
                     </div>
                   </div>
                   <div class="large-4 cell">
                     <div class="btn-wrap learn-more" style="margin-top: auto">
               <a href="https://webdesignbee.com/make-website-load-blazing-fast/" class="button">Read More</a>
             </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>

         </div>
       </div>
      </section>
		    </main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
