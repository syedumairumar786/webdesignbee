<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="content">

	<div class="inner-content grid-x grid-margin-x grid-padding-x">

		<main class="main large-12 cell" role="main">

			<!-- Main Section -->
			 <section class="section" id="main">
			 <div class="grid-container">
				 <div class="grid-x grid-margin-x grid-padding-x">
					 <div class="large-4 cell"  >

						<div class="sticky" data-sticky data-margin-top="6" data-anchor="post-wrap" data-sticky-on="large">
							<div class="section-title">
								<h3 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h3>
							</div>
							<?php get_template_part( 'parts/content', 'cats' ); ?>
							<div class="section-content">
								<h5>About the Author</h5>
								<p><?php the_author_meta( 'description' , $post->post_author ); ?> </p>

							</div>
							<div class="sharing">

							 <p>Share on <a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?> <?php the_permalink(); ?> via @TheWebdesignbee">Twitter</a> or <a href="https://smashing-delivery.herokuapp.com/ball?uri=//www.linkedin.com/shareArticle?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>">LinkedIn</a></p>
							 <p><?php get_template_part( 'parts/content', 'date' ); ?></p>
							 <p><a href="#comments">Leave a comment</a></p>
							</div>
						</div>
					 </div>
					 <div class="large-8 cell" id="post-wrap">
						 <section id="articles" class="section" style="padding-top: 0;">
 							<div class="grid-container">
 							 <div class="grid-x grid-padding-x grid-margin-x">
 								 <div class="large-12 cell">
 									 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

 									<!-- To see additional archive styles, visit the /parts directory -->
 									<?php get_template_part( 'parts/loop', 'single' ); ?>

 								<?php endwhile; else : ?>

 									<?php get_template_part( 'parts/content', 'missing' ); ?>

 								<?php endif; ?>
 								 </div>
 							 </div>
 							</div>
 						</section>
					 </div>
				 </div>
			 </div>
			 </section>
			 <!-- Main Section Ends -->


			<section id="service">
			 <div class="grid-container">
				 <div class="grid-x grid-margin-x grid-padding-x">
					 <div class="large-12 cell">
						 <div class="section-info">
							 <div class="section-title">
								 <h2>Website running slow?</h2>
							 </div>
							 <div class="section-content">
								 <div class="grid-x">
									 <div class="large-8 cell">
										 <div class="service-content">
											 <p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
										 </div>
									 </div>
									 <div class="large-4 cell">
										 <div class="btn-wrap learn-more" style="margin-top: auto">
							<a href="https://webdesignbee.com/make-website-load-blazing-fast/" class="button">Read More</a>
						 </div>
									 </div>
								 </div>
							 </div>
						 </div>
					 </div>

				 </div>
			 </div>
			</section>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
