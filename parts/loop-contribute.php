<?php
/**
 * Template part for displaying page content in page.php
 */
?>


<!-- Main Section -->
<section class="section" id="main">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-5 cell">
			<div class="section-title">
				<h3>Contribute</h3>
			</div>
			<div class="section-content">
				<p>Since Webdesignbee has just started, I am looking for contributors from all over the world. If you’re willing to be an author at my site, <a href="mailto:syedumairumar786@gmail.com" style="color: #fff; border-bottom: 1px dashed;">Contact me here</a>.</p>
			</div>
		</div>
		<div class="large-7 cell">
			<div class="side-img show-for-large">
				<div class="side-slides">
    <div class="side-slide">
        <div class="side-slide-icon">
            <span class="icon-heart"></span>
        </div>
    </div>
</div>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Main Section Ends -->

<!-- About Section Begins -->
<section class="section" id="made-with-love">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-5 cell">
			<div class="section-info">
				<div class="section-title">
					<h3>Let's write cool stuff, together!</h3>
				</div>
				<div class="section-content">
				 <p>As an author, you’ll be able to write posts for the Website, and you’ll have your bio listed below every post you write. I would like to make my site one of the best resources for Web Design on the internet, and am looking for others to help me achieve my mission. I’m a new blogger <img draggable="false" class="emoji" alt="🙂" src="https://s.w.org/images/core/emoji/11/svg/1f642.svg"></p>
				 <p>Just dreamed of having a blog one day, working hard on it and eventually making it one of the World’s best Web Design blog on the planet. Blogging has become my hobby now. As I blog more, I get better and&nbsp; better every day. Aside from working on projects for others, I like writing blog posts at my free time for my very own blog. It improves my writing skills.</p>
				 <p>I also have my own personal Website, <a href="http://syedumair.com">syedumair.com</a>. I have wrote a few posts in there as well, but I am mostly going to work on this blog. I just wish that things go right and everything goes as planned
			</div>
			</div>
		</div>
	</div>

</section>

<!-- Services Section Begins -->

<section id="service">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-12 cell">
				<div class="section-info">
					<div class="section-title">
						<h2>Website running slow?</h2>
					</div>
					<div class="section-content">
						<div class="grid-x">
							<div class="large-8 cell">
								<div class="service-content">
									<p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
								</div>
							</div>
							<div class="large-4 cell">
								<div class="btn-wrap learn-more" style="margin-top: auto">
					<a href="https://webdesignbee.com/make-website-load-blazing-fast/" class="button">Read More</a>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
