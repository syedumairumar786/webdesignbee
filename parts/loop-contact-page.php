<?php
/**
 * Template part for displaying page content in page.php
 */
?>


<!-- Main Section -->
<section class="section" id="main">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-5 cell">
			<div class="section-title">
				<h3>What's up, beautiful?</h3>
			</div>
			<div class="section-content">
<p>You probably already know that we are always looking to help others. It is one of the reason to why we started Webdesignbee.</p>
If you need help with something, want to discuss a project or simply willing to provide some feedback about our site just, feel free to <a href="mailto:syedumairumar786@gmail.com" style="color: #fff; border-bottom: 1px dashed;">write us</a>.</p>
			</div>
		</div>
		<div class="large-7 cell">
			<div class="side-img show-for-large">
				<div class="side-slides">
    <div class="side-slide">
        <div class="side-slide-icon">
            <span class="icon-map-marker"></span>
        </div>
    </div>
</div>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Main Section Ends -->

<!-- About Section Begins -->
<section class="section" id="made-with-love">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-5 cell">
			<div class="section-info">
				<div class="section-title">
					<h3>Where are we based?</h3>
				</div>
				<div class="section-content">
<p>We currently reside in an EC2 instance of Amazon AWS.</p>
<p>Since we don't officially own a physical place that we could call "Our Office", we are working from a wonderful place called home. But we promise to have an official space probably very soon in the near future :)</p>
				</div>
			</div>
			</div>
		</div>
	</div>

</section>

<!-- Services Section Begins -->

<section id="service">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-12 cell">
				<div class="section-info">
					<div class="section-title">
						<h2>Website running slow?</h2>
					</div>
					<div class="section-content">
						<div class="grid-x">
							<div class="large-8 cell">
								<div class="service-content">
									<p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
								</div>
							</div>
							<div class="large-4 cell">
								<div class="btn-wrap learn-more" style="margin-top: auto">
					<a href="https://webdesignbee.com/make-website-load-blazing-fast/" class="button">Read More</a>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
