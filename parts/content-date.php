<?php
/**
 * The template part for displaying an author byline
 */
?>

<p class="date">
	<?php the_time('F j, Y') ?>
</p>
