<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<!-- Main Section -->
<section class="section" id="main">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-5 cell">
			<div class="section-title">
				<h3>How did it all began?</h3>
			</div>
			<div class="section-content">
<p>We are 4 siblings, 3 of us freelancing for half of a decade. We create Websites, logos and animated videos. Our focus has been to help as many people as we can reach. In order to reach more people, we decided to create Webdesignbee.</p>
			</div>
		</div>
		<div class="large-7 cell">
			<div class="side-img float-right show-for-large">
				<img src="<?php bloginfo('template_directory'); ?>/assets/images/syed.png" alt="Syed Umair Umar">
			</div>
		</div>
	</div>
</div>
</section>
<!-- Main Section Ends -->

<!-- About Section Begins -->
<section class="section" id="made-with-love">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-5 cell">
			<div class="section-info">
				<div class="section-title">
					<h3>What makes us unique?</h3>
				</div>
				<div class="section-content">
<p>Hello there! I am Umair. One of the co-founders of <strong>Webdesignbee</strong>. It took us a while to realise that the best thing we can do to try to make the world a better place to live, is to help as many people as we can. In any way possible. For us, in that case, our design and development skills really come handy. So now you know the sole purpose to why we are doing what we are doing, we are always looking to help you!</p>				</div>
			</div>
			</div>
		</div>
	</div>
</section>

<!-- Team Intro Section -->
<section class="section" id="team-intro">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-6 cell">
			<div class="section-title">
				<h3>Team of thrillers</h3>
			</div>
			<div class="section-content">
<p>When 3 siblings work together, it's a fun ride. I am amazed to see my sisters always being there for me, every single time I require a video animated or need a logo done. Not only do we strive to help the outer world, but also each other. We love helping out! All 3 of us aim to be artists. We are supposed to think out of the box every day, and we love to called "The Perfectionists".</p>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Team Intro Section Ends -->

<!-- Team Members Section -->
<section class="section" id="team-members">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-4 cell">
			<div class="section-title">
				<h3>Umair</h3>
				<p class="position">Founder, Web Developer, Problem Solver</p>
			</div>

		</div>
		<div class="large-8 cell">
			<div class="section-content">
<p>Something Umair really feels proud of is his problem solving skill. Every time he get a new project no matter how complex, he doesn't finds "Cannot" as a choice. It's something helps him stand out in the crowd. Umair loves blogging.</p>
			</div>
		</div>
	</div>
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-4 cell">
			<div class="section-title">
				<h3>Razia</h3>
				<p class="position">Co-Founder, Graphics Designer, Make-up Junkie</p>
			</div>

		</div>
		<div class="large-8 cell">
			<div class="section-content">
<p>Razia is a true workaholic. She spends most of her day creating logos, and other graphic materials for her clients. What makes her different is her strong desire to get work done, no matter how much or how hard. You'll never find her being lazy. Aside from doing work, she enjoys spending her time doing her make-up.</p>
			</div>
		</div>
	</div>
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-4 cell">
			<div class="section-title">
				<h3>Hamna</h3>
				<p class="position">Co-Founder, Graphics Designer, Video Animator, Nature Lover</p>
			</div>

		</div>
		<div class="large-8 cell">
			<div class="section-content">
<p>Hamna loves to help people. She's highly experienced with Adobe After Effects, and white board animations. She's also a solid Graphics Designer, and is capable of creating business stationary.</p>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Team Intro Section Ends -->

<section id="service">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-12 cell">
				<div class="section-info">
					<div class="section-title">
						<h2>Website running slow?</h2>
					</div>
					<div class="section-content">
						<div class="grid-x">
							<div class="large-8 cell">
								<div class="service-content">
									<p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
								</div>
							</div>
							<div class="large-4 cell">
								<div class="btn-wrap learn-more" style="margin-top: auto">
					<a href="http://webdesignbee.com/make-website-load-blazing-fast/" class="button">Read More</a>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
