<?php
/**
 * Template part for displaying page content in page.php
 */
?>


<!-- Main Section -->
<section class="section" id="main">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-5 cell">
			<div class="section-title">
				<h3>Privacy Policy</h3>
			</div>
			<div class="section-content">
				<p>By using our Website, you automatically accept the<br>Privacy Policy found in this page.</p>
			</div>
		</div>
		<div class="large-7 cell">
			<div class="side-img show-for-large">
				<div class="side-slides">
    <div class="side-slide">
        <div class="side-slide-icon">
            <span class="icon-gavel"></span>
        </div>
    </div>
</div>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Main Section Ends -->

<!-- About Section Begins -->
<section class="section" id="made-with-love">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-5 cell">
			<div class="section-info">
				<div class="section-title">
					<h3>An important piece of information, worth reading!</h3>
				</div>
				<div class="section-content">
<p>We do not share personal information with third-parties nor do we store information we collect about your visit to this blog for use other than to analyze content performance through the use of cookies, which you can turn off at anytime by modifying your Internet browser’s settings. We are not responsible for the republishing of the content found on this blog on other Web sites or media without our permission. This privacy policy is subject to change without notice.</p>				</div>
			</div>
			</div>
		</div>
	</div>

</section>

<!-- Services Section Begins -->

<section id="service">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-12 cell">
				<div class="section-info">
					<div class="section-title">
						<h2>Website running slow?</h2>
					</div>
					<div class="section-content">
						<div class="grid-x">
							<div class="large-8 cell">
								<div class="service-content">
									<p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
								</div>
							</div>
							<div class="large-4 cell">
								<div class="btn-wrap learn-more" style="margin-top: auto">
					<a href="http://webdesignbee.com/make-website-load-blazing-fast/" class="button">Read More</a>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
