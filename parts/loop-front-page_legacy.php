<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<!-- Main Section -->
<section class="section" id="main">
<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-5 cell">
			<div class="section-title">
				<h3>We are a team of professional designers, and developers.</h3>
			</div>
			<div class="section-content">
				<p>We would love to do all the dirty-work for you. So that you can have your Website ready to rock by the professionals. Working on everything behind the scenes, ensuring zero hassle.</p>
				<div class="learn-more">
					<a href="#" class="button" style="margin-top: 1rem;">Let's talk!</a>
				</div>
			</div>
		</div>
		<div class="large-7 cell">
			<div class="side-img">
				<img src="http://syed.web/webdesignbee/wp-content/uploads/2018/08/screens.png" alt="Screens">
			</div>
		</div>
	</div>
</div>
</section>
<!-- Main Section Ends -->

<!-- About Section Begins -->
<section class="section" id="made-with-love">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-5 cell">
			<div class="section-info">
				<div class="section-title">
					<h3>We code Websites with <span class="icon-heart"></span></h3>
				</div>
				<div class="section-content">
					<p>Here at <strong>Webdesignbee</strong>, we believe that every Website needs love, just like most of the other living things do in our planet. We therefore give all of it to every single facet of the Website that we code. Be it Search Engine Optimisation, or Design!</p>
				</div>
			</div>
			</div>
		</div>
	</div>
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x text-center">
			<div class="large-4 cell">
				<div class="ico-box-items">
					<div class="ico-box">
						<div class="ico-box-icon">
<span class="icon-rocket"></span>
						</div>
						<div class="ico-box-content">
							<h4>Optimised for Speed</h4>
							<p>A faster Website can help you build a solid reputation over search engines, and that's just why we are here to speed it up for you.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="large-4 cell">
				<div class="ico-box-items">
					<div class="ico-box">
						<div class="ico-box-icon">
<span class="icon-code"></span>
						</div>
						<div class="ico-box-content">
							<h4>Ensuring healthy code</h4>
							<p>We believe that a badly coded Website is no different than a Zombie. Given how both can scare you due to severe chances of infection.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="large-4 cell">
				<div class="ico-box-items">
					<div class="ico-box">
						<div class="ico-box-icon">
	<span class="icon-wordpress"></span>
						</div>
						<div class="ico-box-content">
							<h4>Made in WordPress</h4>
							<p>We feel proud of ourselves for using the platform that powers 31% of the web to build all the sexy Websites for our clients.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Services Section Begins -->
<section class="section" style="margin-bottom: 5rem;">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x" data-equalizer data-equalize-on="medium" id="test-eq">
			<div class="large-10 cell">
			<div class="section-info" data-equalizer-watch>
				<div class="section-title">
					<h3>Wait, there's more!</h3>
				</div>
				<div class="section-content">
					<p>Looking for something more specific than just a Website? We are sure you are going to love our extensive range of services.</p>
				</div>
			</div>
			</div>
			<div class="large-2 cell">
				<div class="btn-wrap" data-equalizer-watch>
					<a href="#" class="button">View Services</a>
				</div>
			</div>
		</div>
		<!--<div class="grid-x grid-margin-x grid-padding-x services-box">
		<div class="large-3 cell">
				<div class="service-box">
<p class="service-name">Some text here</p>
</div>
			</div><div class="large-3 cell">
				<div class="service-box">
<p class="service-name">Some text here</p>
</div>
			</div><div class="large-3 cell">
				<div class="service-box">
<p class="service-name">Some text here</p>
</div>
			</div><div class="large-3 cell">
				<div class="service-box">
<p class="service-name">Some text here</p>
</div>
			</div>
		</div>-->
	</div>

</section>
<section id="service">
	<div class="grid-container">
		<div class="grid-x grid-margin-x grid-padding-x">
			<div class="large-12 cell">
				<div class="section-info">
					<div class="section-title">
						<h2>Website running slow?</h2>
					</div>
					<div class="section-content">
						<div class="grid-x">
							<div class="large-8 cell">
								<div class="service-content">
									<p>We understand how a slow Website can turn into a real pain, and that's why we offer a varitey of articles describing some of the easiest ways out there to make a Website run faster than you can ever imagine. Looking for more than just articles? We are here to help!</p>
								</div>
							</div>
							<div class="large-4 cell">
								<div class="btn-wrap learn-more" style="margin-top: auto">
					<a href="#" class="button">Browse Articles</a>
				</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
