<?php
/**
 * The template part for displaying an author byline
 */
?>

<p class="byline">
	Posted in <?php the_category(', ') ?>
</p>	
