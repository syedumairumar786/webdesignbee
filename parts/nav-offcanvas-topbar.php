<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="grid-container">
	<div class="grid-x grid-margin-x grid-padding-x">
		<div class="large-12 cell">
			<div class="header-search hide" id="headerSearch" data-toggler=".hide" >
				<div class="search-form-header">
				<?php echo get_search_form(); ?>
				</div>
			</div>
			<div class="top-bar" id="top-bar-menu">
				<div class="top-bar-left float-left">
					<ul class="menu">
						<li><a href="<?php echo home_url(); ?>">
							<img class="logo" width="144" height="35" src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?>">
						</a></li>
					</ul>
				</div>
				<div class="top-bar-right show-for-medium">
					<?php joints_top_nav(); ?>
				</div>
				<div class="top-bar-right float-right show-for-small-only hamburger">
					<ul class="menu">
						<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
						<!-- <li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>-->
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
