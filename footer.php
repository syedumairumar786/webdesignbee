<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>

				<footer class="footer" role="contentinfo">

					<div class="grid-container">
            <div class="inner-footer grid-x grid-margin-x grid-padding-x">

  						<div class="small-12 medium-12 large-12 cell">
                <div class="grid-x">
                  <div class="footer-logo large-2 cell medium-3 small-12">
                    <a href="<?php echo site_url(); ?>">
  <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" alt="Webdesignbee" width="144" height="35">
                    </a>
                  </div>
                  <div class="social-nav-wrap auto cell medium-4 small-12">
                    <ul class="social-links">
                      <li>
                        <a href="https://www.facebook.com/webdesignbee/" target="_blank"><span class="icon-facebook-square"></span></a>
                      </li>
                      <li>
                        <a href="https://www.twitter.com/TheWebdesignbee/"><span class="icon-twitter-square"></span></a>
                      </li>
                      <li>
                        <a href="https://www.instagram.com/webdesignbee/"><span class="icon-instagram"></span></a>
                      </li>
                    </ul>
                  </div>
    							<nav role="navigation cell large-3 medium-4 small-12">
    	    						<?php joints_footer_links(); ?>
    	    					</nav>
                </div>
  	    				</div>



  					</div> <!-- end #inner-footer -->
          </div>
<div class="copyright">
<div class="grid-container">
  <div class="grid-x grid-margin-x grid-padding-x">
    <div class="small-12 medium-12 large-12 cell">
    <div class="grid-x">
      <div class="auto cell medium-6 small-6">
        <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
      </div>
      <div class="large-3 medium-6 small-6 cell">
        <nav role="navigation" class="float-right">
            <?php joints_subfooter_links(); ?>
          </nav>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
				</footer> <!-- end .footer -->

			</div>  <!-- end .off-canvas-content -->

		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
